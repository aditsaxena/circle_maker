CIRCLE MAKER



INSTALLATION

To install and run the app:

    $ brew install qt
    $ bundle
    $ ruby app_starter.rb
    
Please note on OsX the application window does not come automatically in foreground.  
To bring it on top of other windows just click it's active icon.


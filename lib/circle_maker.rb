require 'Qt'

require './lib/circle_maker/color'
require './lib/circle_maker/circle_drawer'
require './lib/circle_maker/letter_provider'
require './lib/circle_maker/app'

module CircleMaker

  def self.app_runner
    app = Qt::Application.new ARGV
    CircleMaker::App.new
    app.exec
  end

end



module CircleMaker

  class Color
    FILL_GRAY = Qt::Color.new(171, 171, 171)
    FILL_RED = Qt::Color.new(242, 108, 79)
    BLACK = Qt::Color.new(0, 0, 0)
  end

end
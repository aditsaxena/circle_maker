module CircleMaker

  class LetterProvider

    attr_reader :letter_attributes

    def initialize(text_input)
      @letter_attributes = []
      @text_input = text_input
    end

    def snapshot_letter(x, y)
      input_string = @text_input.text
      letter = input_string.slice!(0)
      return unless letter

      @text_input.text = input_string
      @letter_attributes << {
        letter: letter,
        x: x,
        y: y,
      }
    end
  end

end

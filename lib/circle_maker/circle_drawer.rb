module CircleMaker

  class CircleDrawer

    RADIUS = 40

    def initialize(painter)
      @painter = painter
    end

    def draw_circles(letter_attributes)
      @painter.setBrush Qt::Brush.new(Color::FILL_RED)

      letter_attributes.each do |letter_attribute|
        draw_circle letter_attribute
        draw_letter letter_attribute
      end
    end

    private

    def draw_circle(letter_attribute)
      @painter.setPen Qt::NoPen
      x = letter_attribute[:x] - RADIUS/2.0
      y = letter_attribute[:y] - RADIUS/2.0
      @painter.drawEllipse x, y, RADIUS, RADIUS
    end

    def draw_letter(letter_attribute)
      @painter.setPen Color::BLACK
      @painter.drawText letter_attribute[:x] - 5,
                        letter_attribute[:y] + 5,
                        letter_attribute[:letter]
    end

  end

end
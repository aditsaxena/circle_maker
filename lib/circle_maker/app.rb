module CircleMaker

  class App < Qt::MainWindow

    def initialize
      super

      @letter_attributes = []

      setWindowTitle "Circle Maker"

      setPalette Qt::Palette.new(Color::FILL_GRAY)
      setAutoFillBackground(true)

      init_ui

      resize 350, 280
      move 300, 300

      show
    end

    def paintEvent(event)
      painter = Qt::Painter.new self
      painter.setRenderHint Qt::Painter::Antialiasing
      CircleDrawer.new(painter).draw_circles(@letter_provider.letter_attributes)
      painter.end
    end

    def init_ui
      @text_input = Qt::LineEdit.new self
      @text_input.move 20, 20

      @letter_provider = LetterProvider.new(@text_input)
    end

    def mousePressEvent(event)
      return unless event.button() == Qt::LeftButton
      @letter_provider.snapshot_letter(event.x, event.y)
      repaint
    end

  end

end

